# Notatniki do zajęć

Niniejsze repozytorium zawiera zbiór notatników dla _[jupyter-notebook](https://jupyter.org/)_, które wykorzystuję na swoich zajęciach. Notatniki łączą w sobie możliwość przekazywania treści w formie tekstu wespół z kodem, który można interaktywnie uruchomić. 

_Jupyter-notebook_ jest w istocie zbiorem narzędzi, które są udostępnione w formie aplikacji webowej. Aby ułatwić jego instalację oraz konfigurację udostępniłem specjalnie przygotowany obraz dla _Docker_-a, który zawiera dodatkowo skonfigurowane środowisko uruchomieniowe dla _Node.js_ oraz _C++_.

## Instalacja środowiska

Ze względu na dużą przenośność obrazów przy pomocy plików _Dockerfile_, jak również ze względu na działanie samego _Docker_-a instalacja środowiska wymaga jedynie kilku narzędzi.

### _Krok 1_ - Instalacja _GIT_-a (jednorazowo)

Najlepiej kierować się [oficjalną dokumentacją](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

### _Krok 2_ - Instalacja _Docker_-a (jednorazowo)

Najlepiej kierować się [oficjalną dokumentacją](https://docs.docker.com/v17.09/engine/installation/). Warto zaznaczyć, że dla systemów Windows innych niż Windows 10 PRO (szczegóły [tutaj](https://docs.docker.com/v17.09/docker-for-windows/install/#what-to-know-before-you-install)) należy zainstalować starsze rozwiązanie, tj. [Docker Toolbox](https://docs.docker.com/toolbox/toolbox_install_windows/).

#### Uwagi dla _Docker Toolbox_ (opcjonalne)

W przypadku korzystania z _Docker Toolbox_ należy utworzyć maszynę wirtualną o domyślnej nazwie _default_ przy pomocy następującej komendy: `docker-machine create`. Kolejną komendą, tj. `docker-machine status` można sprawdzić czy wirtualna maszyna została prawidłowo uruchomiona. W razie problemów można wykorzystać komendę `docker-machine start` aby uruchomić maszynę. Wszystkie komendy narzędzia zostały opisane [na tej stronie](https://docs.docker.com/machine/reference/).

### _Krok 3_ - Pobranie niniejszego repozytorium

Aby pobrać niniejsze repozytorium należy wpisać w terminalu:
```
git clone https://gitlab.com/ii-us/courses-notebooks.git
```
Powyższa komenda utworzy na dysku katalog o nazwie `courses-notebooks` zawierający obraz dla _Docker_-a (w formie pliku _Dockerfile_) oraz posegregowane w katalogach pliki _.ipynb_, które są instrukcjami do zajęć.

#### Zachowywanie efektów pracy

Wielką zaletą plików _.ipynb_ jest fakt, że są one przenośne, oznacza to, że po ukończonych zajęciach studenci mogą zapisać efekt swojej pracy (np. przesłać na skrzynkę mailową lub pamięć przenośną), by następnie uruchomić zachowane dane w domu lub wykorzystać je na kolejnych zajęciach.

Kolejnym dobrym sposobem zachowana efektów pracy jest wykorzystanie repozytorium _GIT_ poprzez utworzenie _fork_-u niniejszego repozytorium lub utworzenie nowego repozytorium, które przechowywać będzie zmodyfikowane pliki _.ipynb_.

### _Krok 4_ - Budowa obrazu zawierającego narzędzie _jupyter-notebook_

Po przejściu do folderu zawierającego pobrane repozytorium należy uruchomić następującą komendę, która zbuduje obraz środowiska (może trwać kilka minut):
```
docker image build --force-rm -t jupyter/courses-notebook:latest .
```

#### Uwagi dla _Docker Toolbox_ (opcjonalne)

Jeżeli używany jest _Docker Toolbox_ użytkownik nie ma bezpośredniego dostępu do _działających_ komend _Docker_-a, wówczas najprostszą opcją (chodź oczywiście niejedyną) jest dodanie dostępu do katalogu z repozytorium do maszyny wirtualnej, o czym można przeczytać w [oficjalnej dokumentacji](https://docs.docker.com/toolbox/toolbox_install_windows/#optional-add-shared-directories).

Dodanie współdzielonego katalogu wymaga restartu maszyny wirtualnej co można uczynić za pomocą następującej komendy: `docker-machine restart`.

Następnie przy pomocy komendy `docker-machine ssh` logujemy się do maszyny wirtualnej, gdzie przechodzimy do współdzielonego folderu z plikami repozytorium i wykonujemy powyższe polecenia (i wszystkie poniższe).

## Uruchomienie środowiska

Aby uruchomić zawartość kursów należy najpierw zbudować obraz środowiska dla _Docker_-a, by następnie uruchomić serwer aplikacji i uzyskać dostęp do edytora.

### _Krok 5_ - Uruchomienie serwera _jupyter-notebook_

Aby uruchomić serwer _jupyter-notebook_ wykonaj następującą komendę:
```
docker run -it --rm -p 8888:8888 -v courses-notebooks:/home/jovyan/workspace jupyter/courses-notebook:latest
```
Prawidłowo uruchomione środowisko powinno zakończyć się wyświetleniem adresów dostępowych do narzędzia, przykładowo:
```
http://9eeebd80cb57:8888/?token=0ce5fd3e6b2ee6a0703c9335305784d6101b8946df163478
	or http://127.0.0.1:8888/?token=0ce5fd3e6b2ee6a0703c9335305784d6101b8946df163478
```

### _Krok 6_ - Uruchomienie edytora _jupyter-notebook_

Jeżeli serwer działa prawidłowo wystarczy wpisać w przeglądarkę adres otrzymany w poprzednim kroku, np.:
```
http://127.0.0.1:8888/?token=0ce5fd3e6b2ee6a0703c9335305784d6101b8946df163478
```
Uwaga, pole _token_ jest generowane na nowo z każdym uruchomienie. Opcjonalnie _token_ można wpisać w formularzu, który dostępny jest po wejściu na adres `http://127.0.0.1:8888`.

#### Uwagi dla _Docker Toolbox_ (opcjonalne)

Jeżeli używany jest _Docker Toolbox_ IP serwera należy pobrać za pomocą komendy `docker-machine ip`.
